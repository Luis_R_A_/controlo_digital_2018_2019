from tkinter import *
import tkinter as tk
from tkinter import ttk

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class c_livePlot():

    def __init__(self, root, folderObject, fileName, valueNames, numberofSamples = 30, yaxes = [-1000, 1000], row = 0, column = 0, hasX = False):
        self.root = root
        self.folderObject = folderObject
        self.fileName = fileName
        self.hasX = hasX

      #------------------ Liveplot related --------------------------------------------

        #self.file = None
        self.fig = plt.figure()

        self.startRow = row
        self.startColumn = column
        self.root.grid_rowconfigure(8+self.startRow, weight=1)
        self.root.grid_columnconfigure(0+self.startColumn, weight=1)

        self.canvas = FigureCanvasTkAgg(self.fig, self.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(row = 8+self.startRow, column=0 +self.startColumn, sticky = E+S+N+W, rowspan=20, columnspan = 10)

        #self.folderName = self.folderObject.folderName
        #self.filecsv123 = folderName + "/" + self.fileName_IMKF + ".csv"



        toolbarFrame = Frame(master=self.root)
        toolbarFrame.grid(row=7+self.startRow,column=0+self.startColumn)
        toolbar = NavigationToolbar2Tk(self.canvas,toolbarFrame)
        toolbar.update()
        #self.canvas._tkcanvas.grid(row = 0, column = 3)

        self.ax1 = self.fig.add_subplot(1,1,1, picker=True)


        self.graphEnable = False
        #self.c1 = self.fig.canvas.mpl_connect('motion_notify_event', self.graphOnMove)
        #self.c2 = self.fig.canvas.mpl_connect('button_press_event', self.enableGraphMove)
        #self.c3 = self.fig.canvas.mpl_connect('button_release_event', self.disableGraphMove)

        self.numberOfSamples = numberofSamples
        self.numberOfX = len(valueNames)
        self.ax1.clear()
        self.ax1.set_ylim(yaxes)
        self.line = [ [] for _ in range(self.numberOfX)]
        self.ys = [ [0]*self.numberOfSamples for _ in range(self.numberOfX)]
        self.xs = list(range(-self.numberOfSamples, 0))
        k = 0
        #valueNames = ["W", "X", "Y", "kAverage", "gyroWeight"]
        #self.multipliersGraph = [1000, 1000, 1000, 1000, 100]
        for n in self.ys:
            self.line[k], = self.ax1.plot(self.xs, n, label = valueNames[k])
            #print(self.line)
            k += 1
        #self.ax1.legend()

        self.ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=5)

        self.entryNumberSamples = tk.Entry(self.root)
        self.entryNumberSamples["textvariable"] = self.numberOfSamples
        self.entryNumberSamples.insert(0,self.numberOfSamples)
        self.entryNumberSamples.bind('<Key-Return>', self.updateNumberSamples)
        self.entryNumberSamples.grid(row=0+self.startRow,column=0+self.startColumn, sticky = E+W)
        self.labelNumberSamples = tk.Label(self.root, text = "Samples on screen")
        self.labelNumberSamples.grid(row=1+self.startRow,column=0+self.startColumn, sticky = E+W)


        self.refreshRate = 10


        self.entryRefreshRate = tk.Entry(self.root)
        #self.entryRefreshRate["textvariable"] = self.refreshRate
        self.entryRefreshRate.insert(0,self.refreshRate)
        self.entryRefreshRate.bind('<Key-Return>', self.updateRefreshRate)
        self.entryRefreshRate.grid(row=2+self.startRow,column=0+self.startColumn, sticky = E+W)
        self.labelRefreshRate = tk.Label(self.root, text = "Refresh rate (60hz max)")
        self.labelRefreshRate.grid(row=3+self.startRow,column=0+self.startColumn, sticky = E+W)

        self.buttonScroll = ttk.Button(self.root,text="Start/Stop scroll", command=self.toggleScroll)
        self.buttonScroll.grid(row = 4+self.startRow, column=0 +self.startColumn, sticky = N)
        self.automaticScroll = True


        #self.animCounter = 0
        self.ani = animation.FuncAnimation(self.fig, self.animate, fargs=(self.ys,), interval=100, frames=100,  blit=True)
        self.simulator = 0
        self.increment = 5
        self.redraw = False
        #-----------------------END Liveplot related---------------------------------------
        pass


    def animate(self, i, ys):
        self.simulator += self.increment
        if self.simulator >= 1000 or self.simulator <= -1000:
            self.increment = self.increment*-2

        if self.automaticScroll:
            #values = self.folderObject.getCSVValuesFromEnd(self.fileName, nLines = 1, nOfCollumns = 2)
            nColumns = self.numberOfX
            if self.hasX:
                nColumns += 1

            values = self.folderObject.getCSVValuesFromEnd(self.fileName, nLines=self.numberOfSamples, nOfCollumns=nColumns)
            #print(values)
            if self.hasX:
                values = values [1:]
            for k in range(0, self.numberOfX):

                #ys[k].append(values[k])
                ys[k] = (values[k])
                ys[k].extend([0]*(self.numberOfSamples-len(values[k])))
                ys[k].reverse()


                #print(ys[k])

                #ys[k].append(self.simulator * (pow((-1),(k+1))))


                #ys[k] = ys[k][-self.numberOfSamples:]
                self.line[k].set_ydata(ys[k])



            if(self.redraw == True):
                plt.ion()
                self.redraw = False
        return self.line
        pass
    #=======================================
    # update refresh rate
    #=======================================
    def RefreshRate_goGreen(self):
        self.entryRefreshRate.config({"background": "Green"})
    def RefreshRate_goRed(self):
        self.entryRefreshRate.config({"background": "Red"})
    def updateRefreshRate(self, event):
        self.entryRefreshRate.config({"background": "White"})
        number = self.entryRefreshRate.get()
        try:
            refreshRate = float(number)
            if refreshRate > 0 and refreshRate <= 60:
                self.refreshRate = refreshRate
                self.ani.event_source.interval = 1000/self.refreshRate
                self.root.after(100, self.RefreshRate_goGreen)
            else:
                self.root.after(100, self.RefreshRate_goRed)
        except:
            self.root.after(100, self.RefreshRate_goRed)
            pass
    #=======================================
    # END refresh rate
    #=======================================

    #=======================================
    # update Number of Samples
    #=======================================
    def NumberSamples_goGreen(self):
        self.entryNumberSamples.config({"background": "Green"})
    def NumberSamples_goRed(self):
        self.entryNumberSamples.config({"background": "Red"})
    def updateNumberSamples(self, event):
        self.entryNumberSamples.config({"background": "White"})
        number = self.entryNumberSamples.get()
        try:
            numberOfSamples = int(number)
            if numberOfSamples > 0:
                self.numberOfSamples = numberOfSamples
                self.xs = list(range(-self.numberOfSamples,0))
                #values = self.folderObject.getCSVValuesFromEnd(self.fileName, nLines = self.numberOfSamples, nOfCollumns = 2)
                for k in range(0,self.numberOfX):
                    self.line[k].set_xdata(self.xs)
                    self.ys[k] = ([0]*self.numberOfSamples)
                    #self.ys[k] = values[k]

                self.ax1.set_xlim([-self.numberOfSamples,0])
                self.redraw = True
                #plt.show(block = False)
                self.root.after(100, self.NumberSamples_goGreen)
            else:
                self.root.after(100, self.NumberSamples_goRed)
        except:
            self.root.after(100, self.NumberSamples_goRed)
            pass
    #=======================================
    # END update Number of Samples
    #=======================================


    def toggleScroll(self):
        if self.automaticScroll:
            self.automaticScroll = False
        else:
            self.automaticScroll = True



    def stop(self):
        plt.close('all')
