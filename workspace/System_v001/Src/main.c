/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "MPU6050.h"
#include "stdio.h"
#include "math.h"
#include "logging.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart3;

osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM2_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM3_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART3_UART_Init();
  MX_TIM2_Init();
  MX_ADC1_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 2048);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_I2C1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_HSI;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00502F47;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /**I2C Enable Fast Mode Plus 
  */
  HAL_I2CEx_EnableFastModePlus(I2C_FASTMODEPLUS_I2C1);
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 269999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM2;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 9;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 32399;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OnePulse_Init(&htim3, TIM_OPMODE_SINGLE) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 1000000;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD3_Pin|GPIO_PIN_15|LD2_Pin|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_PowerSwitchOn_GPIO_Port, USB_PowerSwitchOn_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin : BUTTONPIN_Pin */
  GPIO_InitStruct.Pin = BUTTONPIN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(BUTTONPIN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SENSOR_INT_Pin */
  GPIO_InitStruct.Pin = SENSOR_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SENSOR_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_MDC_Pin RMII_RXD0_Pin RMII_RXD1_Pin */
  GPIO_InitStruct.Pin = RMII_MDC_Pin|RMII_RXD0_Pin|RMII_RXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_REF_CLK_Pin RMII_MDIO_Pin RMII_CRS_DV_Pin */
  GPIO_InitStruct.Pin = RMII_REF_CLK_Pin|RMII_MDIO_Pin|RMII_CRS_DV_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : RMII_TXD1_Pin */
  GPIO_InitStruct.Pin = RMII_TXD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(RMII_TXD1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD3_Pin PB15 LD2_Pin PB8 */
  GPIO_InitStruct.Pin = LD3_Pin|GPIO_PIN_15|LD2_Pin|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = USB_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OverCurrent_Pin */
  GPIO_InitStruct.Pin = USB_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PC6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : RMII_TX_EN_Pin RMII_TXD0_Pin */
  GPIO_InitStruct.Pin = RMII_TX_EN_Pin|RMII_TXD0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART3 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}
int _write( int file, char *ptr, int len ){


    int i;




    for(i = 0 ; i < len ; i++){


        HAL_UART_Transmit(&huart3,(uint8_t*)&ptr[i],1,10);


    }




    char ch='\r';


    HAL_UART_Transmit(&huart3,(uint8_t*)&ch,1,10);

    return 0;
}



double F_Pitch = 0;
double F_Roll = 0;
double A_Pitch = 0;
double A_Roll = 0;
double Pitch = 0, Roll = 0;
double dt = 0.001;
int32_t Ygyro_offset = -15;

double wMax = 3.14159265359 * 0.1, angleMax = 3.14159265359/10.0;
double p_w_X = 0, p_w_Y = 0, p_w_Z = 0;

#include "filter1.h"
filter1Type *filterHandler_AX = NULL;
filter1Type *filterHandler_AY = NULL;
filter1Type *filterHandler_AZ = NULL;
int32_t calculatePositions(double _scaleGyroLSB, double _scaleAccLSB, double *_Pitch, double *p1, double *p2)
{

	//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6,1);
	int16_t Xacc, Yacc, Zacc, Xgyro, Ygyro, Zgyro;
	MPU6050_Get_Raw_Accel(&Xacc, &Yacc, &Zacc);
	MPU6050_Get_Raw_Gyro(&Xgyro, &Ygyro, &Zgyro);
	MPU6050_Read_Byte_From_Register(INT_STATUS);
	//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6,0);
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8,1);

	//printf("%ld,%ld,%ld,%ld,%ld,%ld\n", Xacc, Yacc, Zacc, Xgyro, Ygyro, Zgyro);

    //double G_X = Xgyro ;
    double G_Y = -(Ygyro - Ygyro_offset) ; //change signal to match
    //double G_Z = Zgyro ;

    double A_X1 = ((double)Xacc * _scaleAccLSB);
    double A_Y1 = ((double)Yacc * _scaleAccLSB);
    double A_Z1 = ((double)Zacc * _scaleAccLSB);

   /*filter1_writeInput( filterHandler_AX, A_X1 );
    double A_X2 = filter1_readOutput( filterHandler_AX );
    filter1_writeInput( filterHandler_AY, A_Y1 );
    double A_Y2 = filter1_readOutput( filterHandler_AY );
    filter1_writeInput( filterHandler_AZ, A_Z1 );
    double A_Z2 = filter1_readOutput( filterHandler_AZ );

    double pitchAcc2 = asin(A_X2/sqrt(pow(A_X2, 2)+ pow(A_Z2,2)+pow(A_Y2,2))); //* 180.0/3.14159265359;
    *p2 = pitchAcc2;*/

    double pitchAcc1 = asin(A_X1/sqrt(pow(A_X1, 2)+ pow(A_Z1,2)+pow(A_Y1,2))); //* 180.0/3.14159265359;
    //double rollAcc = atan2(A_X1, A_Z1) * 180.0 / 3.14159265359;
    *p1 = pitchAcc1;




    //Add angular displacement, in degrees, from gyro data
    //double w_X = _scaleGyroLSB * G_X;

    double w_Y = _scaleGyroLSB * G_Y * 3.14159265359 / 180.0;
    if (w_Y < 0.002 && w_Y  > -0.002)
    	w_Y = 0;
    double anglePitch_gyro = (dt * w_Y);
    //*p1 += anglePitch_gyro;


	//double lastPitch = F_Pitch;
   // F_Roll += (dt * w_X);
	//double temp = F_Pitch;
	F_Pitch += anglePitch_gyro;

    //F_Roll = 0.98 * F_Roll + 0.02 * rollAcc;
	F_Pitch = 0.995 * F_Pitch + 0.005 * pitchAcc1;


    //convert to radians/s
    //Roll = F_Roll;// * 3.14159265359 /180.0;

    Pitch = F_Pitch;// * 3.14159265359 /180.0;


    *_Pitch = Pitch;

    return 0;
}
double p1, p2;
void setGyroAngleRef()
{
	p1 = 0;
	F_Pitch = 0;
}
double ESC_PERIOD = 2.5;
void servoSetCH3(double _ms)
{
	if(_ms > 2.0)
		_ms = 2.0;
	else if(_ms < 1.0)
		_ms = 1.0;

	uint32_t pulse = 0;

	uint32_t ticks20ms = htim2.Instance->ARR;

	pulse = ticks20ms*(_ms/ ESC_PERIOD);



	htim2.Instance->CCR3 = pulse;
}
void servoSetCH4(double _ms)
{
	if(_ms > 2.0)
		_ms = 2.0;
	else if(_ms < 1.0)
		_ms = 1.0;

	uint32_t pulse = 0;

	uint32_t ticks20ms = htim2.Instance->ARR;

	pulse = ticks20ms*(_ms/ ESC_PERIOD);



	htim2.Instance->CCR4 = pulse;
}




int16_t ADC_GetValue(ADC_HandleTypeDef *handler)
{
	if(HAL_ADC_Start(handler) != HAL_OK)
	{
		while(1);
	}

	if(HAL_ADC_PollForConversion(handler,10) != HAL_OK)
	{
		//while(1);
		return -1;
	}

	return HAL_ADC_GetValue(handler);



}
uint32_t tim2Counter = 0;
#define UART_RX_SIZE 255
volatile uint8_t rxBuffer[UART_RX_SIZE];

typedef struct
{
	uint8_t *buffer;
	uint16_t head;
	uint16_t tail;
	uint16_t spaceAvailable;
	uint16_t bytesAvailable;
}s_circularBuffer;

s_circularBuffer rxCircularBuffer = {rxBuffer, 0, 0, UART_RX_SIZE, 0};



void UART_Receive()
{


	  if((__HAL_UART_GET_FLAG(&huart3, UART_FLAG_RXNE) ? SET : RESET) == SET)
	  {
		  uint16_t uhMask;
		  UART_MASK_COMPUTATION(&huart3);
		  uhMask = huart3.Mask;
		  while((__HAL_UART_GET_FLAG(&huart3, UART_FLAG_RXNE) ? SET : RESET) == SET)
		  {
			  if(rxCircularBuffer.spaceAvailable)
			  {
				  rxCircularBuffer.buffer[rxCircularBuffer.head] = (uint8_t)(huart3.Instance->RDR & (uint8_t)uhMask);
				  rxCircularBuffer.head++;
				  rxCircularBuffer.spaceAvailable--;
				  rxCircularBuffer.bytesAvailable++;
				  if(rxCircularBuffer.head > UART_RX_SIZE)
					  rxCircularBuffer.head = 0;
			  }
			  else{
				  volatile uint8_t temp = huart3.Instance->RDR;
			  }
		  }

	  }
}

int32_t bufferReadByte(uint8_t *byte)
{
	if( rxCircularBuffer.bytesAvailable == 0)
		return 0;

	*byte = rxCircularBuffer.buffer[rxCircularBuffer.tail];
	rxCircularBuffer.tail++;
	if(rxCircularBuffer.tail > UART_RX_SIZE)
		rxCircularBuffer.tail = 0;
	rxCircularBuffer.bytesAvailable--;
	rxCircularBuffer.spaceAvailable++;

	return 1;

}


#include <string.h>
double Kp = 0.00, Ki = 0, Kd = 0;
void configGains_callback(uint8_t content[])
{
	//Kp = 0.02;

	volatile s_logging_setKMessage message;
	memcpy((void*)&message, (const void *)content, sizeof(s_logging_setKMessage));

	if(message.mask[0])
	{
		Kp = message.Kp;
	}
	if(message.mask[1])
	{
		Ki = message.Ki;
	}
	if(message.mask[2])
	{
		Kd = message.Kd;
	}

}

uint8_t startUART = 0;
void startStop_callback(uint8_t content[])
{
	startUART = content[0];
}

float baseSpeed = 0;
void speedParameters_callback(uint8_t content[])
{
	s_setSpeedParametersMessage message;
	memcpy((void*)&message, (const void *)content, sizeof(s_setSpeedParametersMessage));
	baseSpeed = message.baseSpeed1;
}

uint8_t multipleControlRate = 10;
void setControlRate_callback(uint8_t content[])
{
	multipleControlRate = content[0];
}

void HardFault_Handler()
{
	while(1);
}


#define MAX_I 1.0
float controlPeriodBase = 0.002;
double PID = 0;
int32_t counterValue = 0;
int32_t watchdogDead = 0;
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN 5 */
	__HAL_TIM_ENABLE_IT(&htim2, TIM_IT_UPDATE);


	int32_t counter = 0;
	float servo1 = 1.0, servo2 = 1.0;
	double servoFront = 1.0, servoBack = 1.0;
	double increment1 = 0.1, increment2 = 0.01;

	int USEPID = 1;
	double I = 0;
	double lastError = 0;

	double Pitch = 0;

	/*filterHandler_AX = filter1_create();
	filterHandler_AY = filter1_create();
	filterHandler_AZ = filter1_create();*/
	//pins being used for i2c1 are PB6 - SCL, PB9 - SDA.
	MPU6050_Init(&hi2c1);
	//volatile uint8_t ID = MPU6050_Get_MyID();
	MPU6050_setGyroDiv(1);
	MPU6050_setDLPF(MPU6050_DLPF_184_188);
	MPU6050_changeGyroScale(MPU6050_SCALEGYRO_2000); //set gyro scale to 2000�/s
	MPU6050_changeAccScale(MPU6050_SCALEACC_2);

	servoSetCH3(1.0);
	servoSetCH4(1.0);
	if (HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3) != HAL_OK)
	{
		/* PWM generation Error */
		while(1);
	}
	if (HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4) != HAL_OK)
	{
		/* PWM generation Error */
		while(1);
	}




	logging_registerCallback(&configGains_callback, logging_setKMessage);
	logging_registerCallback(&startStop_callback, logging_startStopMessage);
	logging_registerCallback(&speedParameters_callback, logging_setSpeedParametersMessage);
	logging_registerCallback(&setControlRate_callback, logging_setControlRateMessage);
	logging_registerCallback(&setGyroAngleRef, logging_setGyroRef);
	SET_BIT(huart3.Instance->CR1, USART_CR1_RXNEIE);

	while(HAL_GPIO_ReadPin(BUTTONPIN_GPIO_Port, BUTTONPIN_Pin) == GPIO_PIN_RESET && startUART == 0)
	{
		uint8_t byte = 0;
		if(bufferReadByte(&byte))
		{
			if(loggin_parseMessage(byte))
			{

			}
		}
	}
	startUART= 1;


	/* Infinite loop */
	uint8_t reset = 1;
	while(1)
	{

		uint8_t byte = 0;
		if(bufferReadByte(&byte))
		{
			if(loggin_parseMessage(byte))
			{

			}
		}
		__HAL_TIM_SET_COUNTER(&htim3, 0);
		__HAL_TIM_CLEAR_IT(&htim3, TIM_IT_UPDATE);
		HAL_TIM_Base_Start_IT(&htim3);
		watchdogDead = 0;
		if(startUART)
		{
			reset = 1;
			while(startUART)
			{

				uint8_t byte = 0;
				if(bufferReadByte(&byte))
				{
					if(loggin_parseMessage(byte))
					{

					}
				}
				//Gyro sample rate: 8Mhz. Acc sample rate: 1Khz.
				if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_4) && !watchdogDead)
				{
					//kick the dog
					counterValue = __HAL_TIM_GET_COUNTER(&htim3);
					__HAL_TIM_SET_COUNTER(&htim3, 0);
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6,1);
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6,0);
					calculatePositions(scaleGyroLSB[MPU6050_SCALEGYRO_2000], scaleAccLSB[MPU6050_SCALEACC_2], &Pitch, &p1, &p2);
					//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15,0);
					//s_logging_pythonMessage temp = logging_gePythonMessage(p1, p2, 0.0001);
					//logging_sendMessage(&huart3, logging_pythonMessage, (uint8_t *)&temp);
				}
				if(watchdogDead)
				{
					watchdogDead = 0;
					HAL_Delay(10);
					MPU6050_Init(&hi2c1);
					//volatile uint8_t ID = MPU6050_Get_MyID();
					MPU6050_setGyroDiv(1);
					MPU6050_setDLPF(MPU6050_DLPF_184_188);
					MPU6050_changeGyroScale(MPU6050_SCALEGYRO_2000); //set gyro scale to 2000�/s
					MPU6050_changeAccScale(MPU6050_SCALEACC_2);
					//start the dog.
					__HAL_TIM_SET_COUNTER(&htim3, 0);
					__HAL_TIM_SET_PRESCALER(&htim3, 9);
					HAL_Delay(10);
					__HAL_TIM_CLEAR_IT(&htim3, TIM_IT_UPDATE);
					HAL_TIM_Base_Start_IT(&htim3);
				}

				//PID part
				if(USEPID && tim2Counter >= multipleControlRate)
				{
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15,1);
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15,0);
					//HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6);
					tim2Counter = 0;
					if(baseSpeed > 1.012)
					{

						double error = 0.0 - Pitch;

						double P = Kp * error;

						//double Kinow = Ki;
						/*if(!(error < 0.314 && error  > - 0.314))
						{
							Kinow = 0;
						}*/
						I += ((Ki * error)*(controlPeriodBase*multipleControlRate));
						if(I > MAX_I)
							I = MAX_I;
						else if(I < -MAX_I)
							I = -MAX_I;
						double D = Kd * (error-lastError) / (controlPeriodBase*multipleControlRate);

						PID = P + I + D;
						/*if (PID > 0.5)
							PID = 0.5;
						else if (PID < -0.5)
							PID = -0.5;*/

						servoFront = baseSpeed - PID;
						servoBack = baseSpeed + PID;

						if(servoFront > 2.0)
							servoFront = 2.0;
						else if(servoFront < 1.2)
							servoFront = 1.2;
						if(servoBack > 2.0)
							servoBack = 2.0;
						else if(servoBack < 1.2)
							servoBack = 1.2;

						servoSetCH3(servoFront);
						servoSetCH4(servoBack);

					}
					else
					{
						servoFront = 1.0;
						servoBack = 1.0;
						I = 0;
						servoSetCH3(1.0);
						servoSetCH4(1.0);
					}
					//s_logging_pythonMessage temp = logging_gePythonMessage(error, PID, 0.0001);
					s_logging_pythonMessage temp = logging_gePythonMessage(Pitch, PID, 0);
					logging_sendMessage(&huart3, logging_pythonMessage, (uint8_t *)&temp);
				}
			}
		}

		if( reset)
		{
			servoSetCH3(1.0);
			servoSetCH4(1.0);
			I = 0;
			reset = 0;
		}

	}
  /* USER CODE END 5 */ 
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM14 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM14) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
