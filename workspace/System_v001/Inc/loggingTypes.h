/*
 * loggingTypes.h
 *
 *  Created on: Nov 1, 2018
 *      Author: Luis Afonso
 */

#ifndef LOGGINGTYPES_H_
#define LOGGINGTYPES_H_

#include "stdint.h"

typedef enum
{
  logging_MotorSpeedMessage             = 0U,
  logging_OrientationMessage 			= 1U,
  logging_GyroMessage					= 2U,
  logging_AccMessage					= 3U,
  logging_setKMessage					= 4U,
  logging_getKMessage					= 5U,
  logging_giveKMessage					= 6U,
  logging_setControlRateMessage			= 7U,
  logging_getControlRateMessage			= 8U,
  logging_giveControlRateMessage		= 9U,
  logging_getSampleRateMessage			= 10U,
  logging_pythonMessage					= 11U,
  logging_startStopMessage				= 12U,
  logging_setSpeedParametersMessage		= 13U,
  logging_setGyroRef					= 14U
} logging_messageTypes;


typedef struct
{
	uint32_t time;
	float speedMotor1;
	float speedMotor2;
}s_logging_MotorsSpeedMessage;

typedef struct
{
	uint32_t time;
	int32_t X;
	int32_t Y;
	int32_t Z;
}s_logging_GyroMessage;

typedef struct
{
	uint32_t time;
	int32_t X;
	int32_t Y;
	int32_t Z;
}s_logging_AccMessage;

typedef struct
{
	uint32_t time;
	float Pitch;
}s_logging_OrientationMessage;

typedef struct
{
	uint32_t time;
	float Pitch;
	float speedMotor1;
	float speedMotor2;
}s_logging_pythonMessage;

typedef struct
{
	uint8_t mask[3];
	uint8_t padding;
	float Kp;
	float Ki;
	float Kd;
}s_logging_setKMessage;


//1 to start, 0 to stop.
typedef struct
{
	uint8_t startStop;
}s_logging_startStopMessage;

typedef struct
{
	float baseSpeed1;
}s_setSpeedParametersMessage;

typedef struct
{
	uint8_t multipleOfRate; //multiples of 2.0833333ms
}s_setControlRateMessage;

typedef struct
{
	uint8_t dummy; //multiples of 2.0833333ms
}s_setGyroRefMessage;

static const uint32_t logging_messagesSizes[] =
{
		sizeof(s_logging_MotorsSpeedMessage), 	//0
		sizeof(s_logging_OrientationMessage), 	//1
		sizeof(s_logging_GyroMessage), 			//2
		sizeof(s_logging_AccMessage),			//3
		sizeof(s_logging_setKMessage),			//4
		0,										//5
		0,										//6
		sizeof(s_setControlRateMessage),		//7
		0,										//8
		0,										//9
		0,										//10
		sizeof(s_logging_pythonMessage),		//11
		sizeof(s_logging_startStopMessage),		//12
		sizeof(s_setSpeedParametersMessage),	//13
		sizeof(s_setGyroRefMessage)				//14
};

typedef void (*logging_callback)(uint8_t content[]);
static logging_callback list_logging_callback[15];
#endif /* LOGGINGTYPES_H_ */
