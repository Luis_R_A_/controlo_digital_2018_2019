

import serial
import socket


class transportSerial():

    def __init__(self, COM, baudrate = '115200', timeout=0):
        self.COM = COM
        self.baudrate = baudrate
        self.timeout = timeout
        try:
            self.ser = serial.Serial(self.COM, self.baudrate, timeout =self.timeout)
            #self.ser.set_buffer_size(rx_size = 12800, tx_size = 12800)
        except serial.SerialException:
            print("Error opening port")
            raise ValueError

        print("Init transportSerial with " + self.COM)

    def __destroy__(self):
        self.ser.close()
    def do_something(self):
        print("hello")


    def pollByte(self):
        return self.ser.read(1)

    def pollBytes(self):
        return self.ser.read(256)

    def sendData(self, data):
        if( isinstance(data, str) ):
            print("sending str")
            counter = 0
            for x in data:
                #print("sending str to int: ", bytes([ord(x)]))
                counter += 1
                self.ser.write(bytes([ord(x)]))
            return counter
        elif(isinstance(data, int)):
            print("sending int ", data)
            self.ser.write(bytes([data]))

        else:
            print("sending not str or int", data)
            self.ser.write(data)



#import datetime
