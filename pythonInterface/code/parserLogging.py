from transport import transportSerial

logging_messagesSizes = [12, 8,  16, 16, 16, 0, 0, 1, 0, 0, 0, 16, 1, 4, 1]

class c_callback():

    def __init__(self):

        print("created generic callback")
        self.sendCounterMax = 10
        #size in bytes
        self.formatsSize = {"int32_t" : 4, "uint32_t" : 4, "float" : 4, "double" : 8, "int64_t" : 8, "int16_t": 2,
                            "uint8_t": 1, "uint32_t_Bits":4}

        #function to convert each data type from bytes
        #uint8 doesn't do anything really.
        self.formatsConversion = {"int32_t" : self.int32_from_bytes, "uint32_t" : self.int_from_bytes,
                             "float" : self.float_from_bytes, "double" : self.double_from_bytes,
                              "int64_t": self.int64_from_bytes,
                              "int16_t": self.int16_from_bytes,
                              "uint8_t": self.uint8_from_bytes,
                              "uint32_t_Bits":self.uint32_Bits_from_bytes}


    def uint32_Bits_from_bytes(self, inBytes):
        res = ""

        value1 = inBytes[3] & 0xFF
        value2 = (inBytes[3] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        value1 = inBytes[2] & 0xFF
        value2 = (inBytes[2] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        value1 = inBytes[1] & 0xFF
        value2 = (inBytes[1] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        value1 = inBytes[0] & 0xFF
        value2 = (inBytes[0] >> 4)
        res += '{0:0004b} '.format(value2)
        res += '{0:0004b} '.format(value1)
        return res

    def int_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8
        return res

    def float_from_bytes(self, inbytes):
        bits = self.int_from_bytes(inbytes)
        mantissa = ((bits&8388607)/8388608.0)
        exponent = (bits>>23)&255
        sign = 1.0 if bits>>31 ==0 else -1.0
        if exponent != 0:
            mantissa+=1.0
        elif mantissa==0.0:
            return sign*0.0
        return sign*pow(2.0,exponent-127)*mantissa

    def double_from_bytes(self,inbytes):
        b = bytes(inbytes)

        value =  str(  struct.unpack('d', b)[0] )#struct.unpack('f', b)
        return value

    def int32_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8

        if res > (2**32)/2:
            res = res - (2**32)
        return res

    def int64_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8

        if res > (2**64)/2:
            res = res - (2**64)
        return res

    def int16_from_bytes(self, inbytes):
        res = 0
        shft = 0
        for b in inbytes:
            res |= b << shft
            shft += 8

        if res > (2**16)/2:
            res = res - (2**16)
        return res

    def uint8_from_bytes(self, inbytes):
        return inbytes[0]

    def callback(self, content, queueInput, queueOutput):
        print("default callback")
        queueOutput.put("==================\n")
        queueOutput.put("default callback\n")
        queueOutput.put("string content")
        queueOutput.put(str(content))
        queueOutput.put("==================")


    def getValue(self, content, format):
        size = self.formatsSize[format]
        value = self.formatsConversion[format](content[:size])

        return [value, size]


    def setSendCounterMax(self, value):
        if type(value) == 'integer':
            self.sendCounterMax = value
        else:
            raise ValueError

    def setFolderName(self, folderObject, file):
        self.sample = 0
        self.folderObject = folderObject
        self.file = file
#====================================




class c_callback_MotorSpeed(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "MotorsSpeed001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float", "float"]

        dataNames = ["time", "speedMotor1", "speedMotor2"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Motors Speed ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend


class c_callback_Gyro(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "Gyro001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "int32_t", "int32_t", "int32_t"]

        dataNames = ["time", "Xgyro", "Ygyro", "Zgyro"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Gyro ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_callback_Acc(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "Acc001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "int32_t", "int32_t", "int32_t"]

        dataNames = ["time", "Xacc", "Yacc", "Zacc"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Acc ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend


class c_callback_Orientation(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "Orientation001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float"]

        dataNames = ["time", "Pitch"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Orientation==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend
class c_callback_pythonMessage(c_callback):

    def __init__(self, logFolder):
        super().__init__()

        self.logFolder = logFolder #c_logFile("MotorsSpeed")
        self.fileName = "pythonMessage001"
        self.file = self.logFolder.createCSV(self.fileName)
        self.sample = 0
        pass


    def runCallback(self, content):

        formatData = ["uint32_t", "float", "float", "float"]

        dataNames = ["time", "Pitch", "M1", "M2"]

        offset = 0
        listData = []
        for x in formatData:
            value, size = self.getValue(content[offset:], x)
            offset += size
            listData.append( str(value) )

        toSend = ""
        toSend += "==============================" + "\n"
        toSend += "== Python Message  ==" + "\n"
        for x, data in zip(dataNames, listData):
            toSend += x + " = " + data + "\n"
        toSend += "==============================" + "\n"

        if self.file:
            if self.sample == 0:
                self.logFolder.logToCSV(self.file, dataNames)

            line = self.logFolder.logToCSV(self.file, listData)

            self.sample+=1

        return toSend

class c_message():

    def __init__(self, type = None, callback = None):

        self.sync1 = 0x01
        self.sync2 = 0x20
        self.type = type
        self.content = []
        self.checksum = 0
        self.counter = 0
        self.callback = callback

    def addContent(self, byte):
        if  self.counter >= logging_messagesSizes[self.type]:
            return -1
        self.counter +=1
        self.content.append(byte)
        if  self.counter >= logging_messagesSizes[self.type]:
            return -1

    def updateCheckSum(self):
        self.checksum = self.sync1
        self.checksum += self.sync2
        self.checksum += self.type
        for x in self.content:
            self.checksum += x

        self.checksum = self.checksum % 256

    def printSelf(self):
        print("type={}\nContent:\n".format(self.type))
        for x in self.content:
            print(x)
        print("checksum={}".format(self.checksum))


from multiprocessing import Process
import multiprocessing
import copy

class c_parser():

    def __init__(self):


        self.knownMessages = None

        self.state = 0;
        self.message = c_message()

        self.queue2Process = multiprocessing.Queue()
        self.queueFromProcess = multiprocessing.Queue()
        self.queueCommands = multiprocessing.Queue()
        self.queueReportFromProcess = multiprocessing.Queue()

    def updateKnownMessages(self, knownMessagesList):
        #print("updating known Messages")
        #self.queue2Process.put(knownMessagesList)
        #print("sent known Messages")
        self.knownMessages = knownMessagesList

    def getMessages(self):
        try:
            message = self.queueFromProcess.get_nowait()
            return message
        except:
            return None

    def createProcess(self):
        self.p = Process(target=self.processTask, args=(self.queueCommands, self.queue2Process, self.queueFromProcess, self.queueReportFromProcess))
        self.p.start()

    def startProcess(self, transport):
        self.queueCommands.put(transport)

    def stopProcess(self):
        self.queueCommands.put("STOP")
        self.p.terminate()


    def sendMessage(self, messageID, content):


        message2Send = c_message()


        message2Send.type = messageID

        for x in content:
            message2Send.addContent(x)


        message2Send.updateCheckSum()


        buff2Send = []
        buff2Send.append(message2Send.sync1)
        buff2Send.append(message2Send.sync2)
        buff2Send.append(message2Send.type)
        for x in message2Send.content:
            buff2Send.append(x)
        buff2Send.append(message2Send.checksum)
        #self.transportHandler.sendData(buff2Send)
        print("content", content)
        print(buff2Send)
        self.queue2Process.put(buff2Send)





    def processTask(self, queueCommands, queue2Process, queueFromProcess, queueReportFromProcess):
        transport = queueCommands.get(block = True, timeout = 999999)
        print("processTask received", transport)


        serialPort = transport[0]
        baudrate = transport[1]
        transportOk = False
        ransportHandler = None
        try:
            transportHandler = transportSerial(serialPort, baudrate)
            transportOk = True
        except:
            pass

        if transportOk:
            queueReportFromProcess.put("OK")


            while(1):

                try:
                    command = queueCommands.get_nowait()
                    if(command == "STOP"):
                        transportHandler = None
                        break
                except:
                    pass

                try:
                    buffer = queue2Process.get_nowait()
                    print("test")
                    transportHandler.sendData(buffer)
                except:
                    pass

                byte = transportHandler.pollByte()
                if byte:
                    byte = int.from_bytes(byte, byteorder='big')
                    message = self.parser(byte)
                    if not message:
                        while byte:
                            byte = transportHandler.pollByte()
                            byte = int.from_bytes(byte, byteorder='big')
                            message = self.parser(byte)
                            if message:
                                queueFromProcess.put(message)
                                break
                                #for x in self.knownMessages:
                                #    if message.type == x.type:
                                        #toSend = x.callback.runCallback(message.content)
                                        #self.tfield.config(state=NORMAL)
                                        #self.tfield.insert("end", toSend)
                                        #self.tfield.see("end")
                                        #self.tfield.config(state=DISABLED)
                                #        break
        pass
    def parser(self, byte):
        #print("byte", byte)
        if self.state == 0 and byte == self.message.sync1:
            #print("found sync1")
            self.state = 1

        elif self.state == 1:
            if byte == self.message.sync2:
                #print("found sync2")
                self.state = 2
                self.message = c_message()
            else:
                self.state = 0
        elif self.state == 2:
            #print("type =", byte)
            if byte >= 0 and byte < 12:
                self.message.type = byte
                self.state = 3
            else:
                self.state = 0
        elif self.state == 3:

            if self.message.addContent(byte) == -1:
                #print("found last byte of content")
                self.state = 4

        elif self.state == 4:
            self.state = 0
            #print("checksum check")
            self.message.updateCheckSum()
            if self.message.checksum == byte:
                return self.message
            else:
                pass
                #print("checksum fail, got", byte, "should be", self.message.checksum)
                #self.message.printSelf()


        return None
