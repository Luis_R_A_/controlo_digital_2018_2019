
#
# pack organizes the widget first https://www.tutorialspoint.com/python/tk_pack.htm
#
#
#

from tkinter import *
import tkinter as tk
from tkinter import ttk

from queue import Queue
from serial.tools import list_ports



from parserLogging import c_parser
import parserLogging


from fileAccess import c_logFile

from livePlot import c_livePlot
import struct


class c_generic_GUI():


    def serialRefresh(self):
        currentSerial = self.serialBox.get()
        serialPorts = []
        serialStillExists = False
        for p in list_ports.comports():
            if p.device == currentSerial:
                serialStillExists = True
            serialPorts.append(p.device)
        self.serialBox['values'] = serialPorts
        if serialStillExists:
            self.serialBox.set(currentSerial)
        else:
            self.serialBox.set("")

        self.root.after(1000, self.serialRefresh)


    def __init__(self,root, controller):

        self.root = root
        self.controller = controller


        #register closing method when window close button is pressed
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

        self.lineCounter_tfieldSysConfig = 0
        #create queues
        #self.queueInput = Queue()
        #self.queueOutput = Queue()


        #self.infinityController = None
        self.serialPort = None
        self.transport = None

        #-----------------------------------------------------
        self.buttonConnect = tk.Button(self.root,text="connect", command=self.connect)
        #self.buttonConnect.pack(side=RIGHT)
        self.buttonConnect.grid(row = 1, column = 0, sticky = E+W)
        self.connected = False
        #-----------------------------------------------------





        #-----------------------------------------------------
        baudrates = ['1000000', '115200', '921600', '1843200', '38400']
        self.baudBox = ttk.Combobox(self.root, values=baudrates, state="readonly", height=4)
        if baudrates != []:
            self.baudBox.set(baudrates[0])
            self.baudrate = baudrates[0]
        self.baudBox.bind("<<ComboboxSelected>>", self._changeBaudrate)
        #self.baudBox.pack(fill='x', side=RIGHT)
        self.baudBox.grid(row = 2, column = 0, sticky = E+W)
        #-----------------------------------------------------




        #-----------------------------------------------------
        serialPorts = []
        for p in list_ports.comports():
             print(p.device)
             serialPorts.append(p.device)
        self.serialBox = ttk.Combobox(self.root, values=serialPorts, state="readonly", height=4)
        if serialPorts != []:
            self.serialBox.set(serialPorts[0])
            self.serialPort = serialPorts[0]
            print("serial defaul port:",self.serialPort)
        self.serialBox.bind("<<ComboboxSelected>>", self._changeSerial)
        #self.serialBox.pack(fill='x', side=RIGHT)
        self.serialBox.grid(row = 3, column = 0, sticky = E+W)
        self.root.after(1000, self.serialRefresh)
        #self.serialBox.grid_remove()
        #-----------------------------------------------------


        #-----------------------------------------------------
        #create text box for system config and board config
        #self.tfield = tk.Text(self.root, width = 70, height = 40)
        #self.tfield.
        #self.tfield.pack()
        #self.tfield.grid(row = 1, column = 1, rowspan=40, sticky = N+S+E+W)
        #self.tfield.config(state=DISABLED)
        #self.tfield.bind("<KeyPress>", self.keydown)
        #self.lineCounter_tfield = 0
        #self.newLineTX = True
        #self.newLineRX = True

        self.labelEntryKp = tk.Label(self.root, text = "Kp")
        self.labelEntryKp.grid(row = 0, column = 1, sticky = S)
        self.entryKp = tk.Entry(self.root)
        self.entryKp.grid(row = 1, column = 1, sticky = N+S+E+W)
        self.entryKp.delete(0,END)
        self.entryKp.insert(0,0.0)

        self.labelEntryKi = tk.Label(self.root, text = "Ki")
        self.labelEntryKi.grid(row = 3, column = 1, sticky = S)
        self.entryKi = tk.Entry(self.root)
        self.entryKi.grid(row = 4, column = 1, sticky = N+S+E+W)
        self.entryKi.delete(0,END)
        self.entryKi.insert(0,0.0)

        self.labelEntryKd = tk.Label(self.root, text = "Kd")
        self.labelEntryKd.grid(row = 5, column = 1, sticky = S)
        self.entryKd = tk.Entry(self.root)
        self.entryKd.grid(row = 6, column = 1, sticky = N+S+E+W)
        self.entryKd.delete(0,END)
        self.entryKd.insert(0,0.0)

        self.labelEntryNeutralValue = tk.Label(self.root, text = "Neutral value")
        self.labelEntryNeutralValue.grid(row = 7, column = 1, sticky = S)
        self.entryNeutralValue = tk.Entry(self.root)
        self.entryNeutralValue.grid(row = 8, column = 1, sticky = N+S+E+W)
        self.entryNeutralValue.delete(0,END)
        self.entryNeutralValue.insert(0,1.0)

        self.labelEntryControlRate = tk.Label(self.root, text = "Control rate (x2.08ms) = {:2.3f}ms".format(2.08333*10))
        self.labelEntryControlRate.grid(row = 11, column = 1, sticky = S)
        self.entryControlRate = tk.Entry(self.root)
        self.entryControlRate.grid(row = 12, column = 1, sticky = N+S+E+W)
        self.entryControlRate.bind('<Return>', self.UpdateControlRate)
        self.entryControlRate.delete(0,END)
        self.entryControlRate.insert(0,10)

        self.buttonSend = tk.Button(self.root,text="send parameters", command=self.sendStuff)
        #self.buttonConnect.pack(side=RIGHT)
        self.buttonSend.grid(row = 14, column = 1, sticky = E+W)
        self.connected = False
        #-----------------------------------------------------

        self.buttonStartStop = tk.Button(self.root,text="Start", command=self.startStop)
        #self.buttonConnect.pack(side=RIGHT)
        self.buttonStartStop.grid(row = 8, column = 0, sticky = E+W)
        self.startStop = "start"

        self.buttonSetGyro0 = tk.Button(self.root,text="Set gyro to 0", command=self.setGyroTo0)
        #self.buttonConnect.pack(side=RIGHT)
        self.buttonSetGyro0.grid(row = 9, column = 0, sticky = E+W)
     #-----------------------------------------------------
        self.logFolder = c_logFile("Folder")

        #self.liveplot = c_livePlot(self.root, self.logFolder , "MotorsSpeed001.csv",["M1", "M2"], column = 8, yaxes = [-50,50], hasX = True)
        self.liveplot = c_livePlot(self.root, self.logFolder , "pythonMessage001.csv",["Error","PID", "0"], row = 20, column = 5, yaxes = [-1,1], hasX = True)

        self.MotorSpeedMessage = parserLogging.c_message(type = 0, callback = parserLogging.c_callback_MotorSpeed(self.logFolder))
        self.OrientationMessage = parserLogging.c_message(type = 1, callback = parserLogging.c_callback_Orientation(self.logFolder))
        self.GyroMessage = parserLogging.c_message(type = 2, callback = parserLogging.c_callback_Gyro(self.logFolder))
        self.AccMessage = parserLogging.c_message(type = 3, callback = parserLogging.c_callback_Acc(self.logFolder))

        self.pythonMessageMessage = parserLogging.c_message(type = 11, callback = parserLogging.c_callback_pythonMessage(self.logFolder))

        self.knownMessages = [self.MotorSpeedMessage, self.OrientationMessage, self.GyroMessage, self.AccMessage, self.pythonMessageMessage]

        self.controller.updateKnownMessages(self.knownMessages)

        self.transportHandler = None


        self.root.after(100, self.pollSerial)


    def setGyroTo0(self):
        toSend = [1];
        self.controller.sendMessage(14,toSend)
    def UpdateControlRate(self, i):
        value = float(self.entryControlRate.get())*2.083333

        self.labelEntryControlRate["text"] = "Control rate (x2.08ms) = {:2.3f}ms".format(value)


    def startStop(self):
        if self.startStop == 'start':
            self.buttonStartStop["text"] = 'Stop'
            self.startStop = 'stop'
            toSend = [1]
            self.controller.sendMessage(12,toSend)
        else:
            self.buttonStartStop["text"] = 'Start'
            self.startStop = 'start'
            toSend = [0]
            self.controller.sendMessage(12,toSend)

    def sendStuff(self):
        Kpval = float(self.entryKp.get())
        Kp = bytearray(struct.pack('f', Kpval))
        Kival = float(self.entryKi.get())
        Ki = bytearray(struct.pack('f', Kival))
        kdval = float(self.entryKd.get())
        Kd = bytearray(struct.pack('f', kdval))

        toSend = [1, 1, 1, 0]
        toSend.extend(Kp)
        toSend.extend(Ki)
        toSend.extend(Kd)
        self.controller.sendMessage(4,toSend)


        toSend = []
        neutralValue = float(self.entryNeutralValue.get())
        neutralValue = bytearray(struct.pack('f', neutralValue))
        toSend.extend(neutralValue)
        self.controller.sendMessage(13,toSend)

        toSend = []
        controlRate = int(self.entryControlRate.get())
        toSend.append(controlRate)
        self.controller.sendMessage(7,toSend)
    #=======================================
    #  int_to_bytes
    #=======================================
    def int_to_bytes(self, number):
        b = []
        k = 0
        while number:
            b.append(number % 256)
            number = int(number / 256)
            k += 1
            if k >= 4:
                return b
        i = k
        for n in range(i,4):
            b.append(0)
            k += 1
        return b
    #=======================================
    # END int_to_bytes
    #=======================================

    #=======================================
    #  _changeSerial
    #=======================================
    def _changeSerial(self,event):
        self.serialPort = self.serialBox.get()
        print("Serial port is now = ", self.serialPort)
        self.serialBox.config({"background": "Green"})
    #=======================================
    # END  _changeSerial
    #=======================================
    #=======================================
    #  _changeBaudrate
    #=======================================
    def _changeBaudrate(self, event):
        self.baudrate = self.baudBox.get()
        print("baudrate is now = ", self.baudrate)
        self.baudBox.config({"background": "Green"})
    #=======================================
    # END _changeBaudrate
    #=======================================



    #=======================================
    # clearConsole
    #=======================================
    def clearConsole(self):
        self.printClear()
        self.print("***Other messages window***\n")
    #=======================================
    # END clearConsole
    #=======================================
    #=======================================
    # connect
    #=======================================
    def connect(self):
        if(self.connected == False):

            if(self.serialPort and self.baudrate):



                #self.transportHandler = transportSerial(self.serialPort, self.baudrate)

                #self.printClear()
                self.transport = [self.serialPort, self.baudrate] # transportSerial( self.serialPort, self.baudrate)
                self.controller.startProcess(self.transport)
                try:
                    print("generic_GUI: initTransport seems okay")
                    self.baudBox['state'] = 'disabled'
                    self.serialBox['state'] = 'disabled'
                    self.connected = True

                   #GUI change aspect
                    self.orig_color = self.buttonConnect.cget("background")
                    self.buttonConnect.configure(bg="Green")
                    self.buttonConnect.config(text='Disconnect')
                except:
               #GUI change aspect
                    self.orig_color = self.buttonConnect.cget("background")
                    self.buttonConnect.configure(bg="Red")
                    print("error in generic GUI when initializing")
            else:
                if not self.serialPort:
                    self.print("Port not set\n")
                if not self.baudrate:
                    self.print("baudrate not set\n")
                self.print("============================\n")
        else:
            self.controller.stopProcess()
            self.baudBox['state'] = 'readonly'
            self.serialBox['state'] = 'readonly'
            self.connected = False
            self.transportHandler = None
            self.buttonConnect.configure(bg=self.orig_color)
            self.buttonConnect.config(text='Connect')

    #=======================================
    # END connect
    #=======================================




    #=======================================
    # on_closing
    #=======================================
    def on_closing(self):
        self.liveplot.stop()
        print("gui bye")
        self.root.destroy()
        if self.connected:
            self.transport = None
    #=======================================
    # END on_closing
    #=======================================



    def pollSerial(self):
        counter = 0
        while counter < 100:
            message = self.controller.getMessages()

            if message:
                #print("found message", message.type)
                for x in self.knownMessages:
                    if message.type == x.type:
                        toSend = x.callback.runCallback(message.content)
                        #self.tfield.config(state=NORMAL)
                        #self.tfield.insert("end", toSend)
                        #self.tfield.see("end")
                        #self.tfield.config(state=DISABLED)
                        break
            else:
                break
            counter += 1

        self.root.after(1, self.pollSerial)






