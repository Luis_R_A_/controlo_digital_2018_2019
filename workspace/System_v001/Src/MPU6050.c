/*
 * MPU6050.c
 *
 *  Created on: Oct 19, 2018
 *      Author: Luis Afonso
 */


#include "MPU6050.h"

/* ===========================================================================
*
*      Low level macros
*
=========================================================================== */
#define I2C_BASE I2C1
#define I2C_BASE_CLOCK RCC_APB1Periph_I2C1

#define SDA_PIN_NUMBER GPIO_PinSource9
#define SCL_PIN_NUMBER GPIO_PinSource8

#define SDA_PIN_AF GPIO_AF_1
#define SCL_PIN_AF GPIO_AF_1
#define SDA_PIN GPIO_Pin_9
#define SCL_PIN GPIO_Pin_8
#define I2C_PIN_PORT GPIOB
#define I2C_GPIO_CLOCK RCC_AHBPeriph_GPIOB

/* ========================================================================*/
/* ===================== End of Low level macros========================== */
/* ========================================================================*/

#define I2C_TIMEOUT         ((uint32_t)0x10000) /*!< I2C Time out */

void _MPU6050_Low_GPIO_Init(void);
void _MPU6050_Low_I2C_Init(int);

//32678
const double scaleGyroLSB[] = {0.00762939453125, 0.0152587890625, 0.030517578125, 0.06103515625};
const double scaleAccLSB[] = {6.1203256013219903298855499112553e-5,  1.2240651202643980659771099822511e-4,2.4481302405287961319542199645021e-4,4.8962604810575922639084399290042e-4 };
/* Accel and Gyro max and min values and the zero offset */
int A_minX=0, A_maxX=0, A_minY=0, A_maxY=0, A_minZ=0, A_maxZ=0;;
int A_X_offset = (422-404)/2, A_Y_offset = (242-584)/2, A_Z_offset = 0;
int G_minX=0, G_maxX=0, G_minY=0, G_maxY=0, G_minZ=0, G_maxZ=0;;
int G_X_offset = (422-404)/2, G_Y_offset = (242-584)/2, G_Z_offset = 0;

/*
  _MPU6050_Low_GPIO_Init
  Initializes the SDA and SCL pins
*/
void _MPU6050_Low_GPIO_Init(){
//  GPIO_InitTypeDef GPIO_InitStructure;
//
//  RCC_AHBPeriphClockCmd(I2C_GPIO_CLOCK, ENABLE);
//
//  GPIO_PinAFConfig(GPIOB, SCL_PIN_NUMBER, SDA_PIN_AF); //SCL
//  GPIO_PinAFConfig(GPIOB, SDA_PIN_NUMBER, SCL_PIN_AF); //SDA

  /* SDA and SCL GPIO config */
//  GPIO_InitStructure.GPIO_Pin = SDA_PIN | SCL_PIN;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//  GPIO_Init(I2C_PIN_PORT, &GPIO_InitStructure);


 /* RCC_AHBPeriphClockCmd(RDY_GPIO_CLOCK, ENABLE);
  GPIO_InitStructure.GPIO_Pin = RDY_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(RDY_PIN_PORT, &GPIO_InitStructure);*/

}

/*
  _MPU6050_Low_I2C_Init
  Initializes the i2c at 100Khz
*/
void _MPU6050_Low_I2C_Init(int _400Khz){
//  I2C_InitTypeDef I2C_InitStructure;

  /* Enable I2C_BASE clock */
//  RCC_APB1PeriphClockCmd(I2C_BASE_CLOCK, ENABLE);

  //RCC_I2CCLKConfig(RCC_I2C_BASECLK_HSI);


  /* I2C_BASE configuration */
//  I2C_StructInit(&I2C_InitStructure);

//if(_400Khz == 1)
//  I2C_InitStructure.I2C_Timing = 0x50330309; //400Khz 0x50330309
//else
//  I2C_InitStructure.I2C_Timing = 0x10420F13; //100Khz /
//  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
//  I2C_InitStructure.I2C_DigitalFilter = 0x00;
//  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
//  I2C_InitStructure.I2C_OwnAddress1 = 0x00;
//  I2C_InitStructure.I2C_Ack = I2C_Ack_Disable;
//  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
//
//  I2C_Init(I2C_BASE, &I2C_InitStructure);
//  I2C_Cmd(I2C_BASE, ENABLE);

}

I2C_HandleTypeDef *i2c_handler;

#define TIMEOUTVALUE 100

/*
  MPU6050_Init
*/
int MPU6050_Init(I2C_HandleTypeDef *_handler){



//  if(_noSetup == 0){
//    int _i2c400Khz=0;
//    if (_speedKhz == 400)
//      _i2c400Khz = 1;
//    /* Low level GPIO and I2C initialization */
//    _MPU6050_Low_GPIO_Init();
//    _MPU6050_Low_I2C_Init(_i2c400Khz);
//  }

	i2c_handler = _handler;

	MPU6050_Send_Byte_To_Register(PWR_MGMT_1, DEFAULT_PWR_MGMT_1);
	MPU6050_Send_Byte_To_Register(INT_PIN_CFG,(1<<5)); //LATCH_INT_EN = 1
	MPU6050_Send_Byte_To_Register(INT_ENABLE,1);

	return 0;

}

int MPU6050_Set_Filters(int _n){

  int _reg = MPU6050_Read_Byte_From_Register(CONFIG);
  _reg &= ~0xF;
  _reg |= _n;
  MPU6050_Send_Byte_To_Register(CONFIG, _reg);

  return 0;
}


/*==============================================================================
  Utility functions to read and write into the registers
==============================================================================*/

int MPU6050_Set_To_Reg(int _n){

//  uint32_t I2C_TimeOut = I2C_TIMEOUT;
//
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_BUSY) != RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_TransferHandling(I2C_BASE, MPU6050_ADDRESS, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);
//
//  /* Send register offset*/
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_TXIS) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_SendData(I2C_BASE, (uint8_t)_n);
//
//  /* STOP */
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_STOPF) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_ClearFlag(I2C_BASE, I2C_ICR_STOPCF);

  HAL_StatusTypeDef returnValue = HAL_I2C_Master_Transmit(i2c_handler, (uint16_t)MPU6050_ADDRESS, (uint8_t*)&_n, 1, TIMEOUTVALUE);

  if(returnValue != HAL_OK)
  {
	  return -1;
  }

  return 0;

}

int MPU6050_Read_Byte_From_Register(int _reg){
  if(MPU6050_Set_To_Reg(_reg) == I2C_TIMEOUT_ERROR){
    return I2C_TIMEOUT_ERROR;
  }

//  uint32_t I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_BUSY) != RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_TransferHandling(I2C_BASE, MPU6050_ADDRESS, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  uint8_t _data = I2C_ReceiveData(I2C_BASE);
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_STOPF) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_ClearFlag(I2C_BASE, I2C_ICR_STOPCF);
  uint8_t _data = 0;
  HAL_StatusTypeDef returnValue = HAL_I2C_Master_Receive(i2c_handler, (uint16_t)MPU6050_ADDRESS, (uint8_t *)&_data, 1, TIMEOUTVALUE);
  if(returnValue != HAL_OK)
  {
	  return -1;
  }

  return _data;
}

int MPU6050_Send_Byte_To_Register(int _reg, uint8_t _data){

//  uint32_t I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_BUSY) != RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_TransferHandling(I2C_BASE, MPU6050_ADDRESS, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  /* Send register offset*/
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_TXIS) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_SendData(I2C_BASE, (uint8_t)_reg);
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_TXIS) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_SendData(I2C_BASE, (uint8_t)_data);
//
//  /* STOP */
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_STOPF) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_ClearFlag(I2C_BASE, I2C_ICR_STOPCF);

  uint8_t sendBuffer[2];
  sendBuffer[0] = _reg;
  sendBuffer[1] = _data;
  HAL_StatusTypeDef returnValue = HAL_I2C_Master_Transmit(i2c_handler, (uint16_t)MPU6050_ADDRESS, (uint8_t*)sendBuffer, 2, TIMEOUTVALUE);


  return 0;
}




char MPU6050_Get_MyID(){
  return MPU6050_Read_Byte_From_Register(WHO_AM_I);
}



/*==============================================================================
  Functions to get output
==============================================================================*/
int MPU6050_Get_Raw_Accel(int16_t* _X, int16_t* _Y, int16_t* _Z){
  if( MPU6050_Set_To_Reg(ACCEL_XOUT_H) == I2C_TIMEOUT_ERROR)
    return I2C_TIMEOUT_ERROR;


//  uint32_t I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_BUSY) != RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_TransferHandling(I2C_BASE, MPU6050_ADDRESS, 6, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_X= (int16_t)(I2C_ReceiveData(I2C_BASE) <<8);
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_X |= (int16_t)(I2C_ReceiveData(I2C_BASE));
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Y= (int16_t)(I2C_ReceiveData(I2C_BASE) <<8);
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Y |= (int16_t)(I2C_ReceiveData(I2C_BASE));
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Z= (int16_t)(I2C_ReceiveData(I2C_BASE) <<8);
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Z |= (int16_t)(I2C_ReceiveData(I2C_BASE));
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_STOPF) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_ClearFlag(I2C_BASE, I2C_ICR_STOPCF);

  uint8_t _data[6];
  //HAL_StatusTypeDef returnValue = HAL_I2C_Master_Receive(&i2c_handler, (uint16_t)MPU6050_ADDRESS, (uint8_t *)_data, 6, 10000);
  HAL_StatusTypeDef returnValue = HAL_OK;
  do{
	  returnValue = HAL_I2C_Master_Receive(i2c_handler, (uint16_t)MPU6050_ADDRESS, (uint8_t *)_data, 6, TIMEOUTVALUE);
  }while(returnValue == HAL_BUSY);
  if(returnValue != HAL_OK)
  {
	  return -1;
  }

  *_X= (int16_t)(_data[0] <<8);
  *_X |= (int16_t)(_data[1]);
  *_Y= (int16_t)(_data[2] <<8);
  *_Y |= (int16_t)(_data[3]);
  *_Z = (int16_t)(_data[4] <<8);
  *_Z |= (int16_t)(_data[5]);

  return 0;
}

int MPU6050_Get_Raw_Gyro(int16_t* _X, int16_t* _Y, int16_t* _Z){
  if(MPU6050_Set_To_Reg(GYRO_XOUT_H) ==   I2C_TIMEOUT_ERROR){
    return I2C_TIMEOUT_ERROR;
  }

//  uint32_t I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_BUSY) != RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_TransferHandling(I2C_BASE, MPU6050_ADDRESS, 6, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_X= (int16_t)(I2C_ReceiveData(I2C_BASE) <<8);
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_X |= (int16_t)(I2C_ReceiveData(I2C_BASE));
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Y= (int16_t)(I2C_ReceiveData(I2C_BASE) <<8);
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Y |= (int16_t)(I2C_ReceiveData(I2C_BASE));
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Z= (int16_t)(I2C_ReceiveData(I2C_BASE) <<8);
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_RXNE) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  *_Z |= (int16_t)(I2C_ReceiveData(I2C_BASE));
//
//  I2C_TimeOut = I2C_TIMEOUT;
//  while(I2C_GetFlagStatus(I2C_BASE, I2C_ISR_STOPF) == RESET) if((I2C_TimeOut--) == 0) return I2C_TIMEOUT_ERROR;
//  I2C_ClearFlag(I2C_BASE, I2C_ICR_STOPCF);
  uint8_t _data[6];
  HAL_StatusTypeDef returnValue = HAL_I2C_Master_Receive(i2c_handler, (uint16_t)MPU6050_ADDRESS, (uint8_t *)_data, 6, TIMEOUTVALUE);
  if(returnValue != HAL_OK)
  {
	  return -1;
  }

  *_X= (int16_t)(_data[0] <<8);
  *_X |= (int16_t)(_data[1]);
  *_Y= (int16_t)(_data[2] <<8);
  *_Y |= (int16_t)(_data[3]);
  *_Z= (int16_t)(_data[4] <<8);
  *_Z |= (int16_t)(_data[5]);
  return 0;
}

int MPU6050_Get_Gyro_Zero(int* _X, int* _Y, int* _Z){
  int _average_X = 0;
  int _average_Y = 0;
  int _average_Z = 0;
  int i;
  int k;
  for(i = 0; i < 50; i++){
    MPU6050_Get_Raw_Gyro(_X, _Y, _Z);
    _average_X += *_X;
    _average_Y += *_Y;
    _average_Z += *_Z;
    for(k=0; k < 160000; k++);
  }
  _average_X = _average_X / i;
  _average_Y = _average_Y / i;
  _average_Z = _average_Z / i;

  return 0;
}

/*==============================================================================
  Interrupts
==============================================================================*/
int MPU6050_Int_Data_Ready_Enable(){
  /* Enables the data ready interrupt without changing the others */
   uint8_t _data = MPU6050_Read_Byte_From_Register(INT_ENABLE);
   _data &= ~0x01;
   _data |= 0x01;
   return MPU6050_Send_Byte_To_Register(INT_ENABLE, _data);
}

int MPU6050_Data_Ready(){

  uint8_t _status = MPU6050_Read_Byte_From_Register(INT_STATUS);

  _status &= 1;

  return _status;
}




/*==============================================================================

  Math

==============================================================================*/

/*
  MPU6050_Update_Accel_Max_Min
  Call this when calibrating offset values to update max and min values of the
accelerometer

  Parameters:
  int _X: Raw value of X
  int _Y: Raw value of Y
  int _Z: Raw value of Z

  Returns: Nothing
*/
void MPU6050_Update_Accel_Max_Min(int _X, int _Y, int _Z){
  if(_X < A_minX)
    A_minX = _X;
  if(_X > A_maxX)
    A_maxX = _X;

  if(_Y < A_minY)
    A_minY = _Y;
  if(_Y > A_maxY)
    A_maxY = _Y;

  if(_Z < A_minZ)
    A_minZ = _Z;
  if(_Z > A_maxZ)
    A_maxZ = _Z;
}



/*
  MPU6050_Update_Accel_Max_Min
  Call this when calibrating offset values to update max and min values of the
accelerometer

  Parameters:
  int _X: Raw value of X
  int _Y: Raw value of Y
  int _Z: Raw value of Z

  Returns: Nothing
*/
void MPU6050_Update_Gyro_Max_Min(int _X, int _Y, int _Z){
  if(_X < G_minX)
    G_minX = _X;
  if(_X > G_maxX)
    G_maxX = _X;

  if(_Y < G_minY)
    G_minY = _Y;
  if(_Y > G_maxY)
    G_maxY = _Y;

  if(_Z < G_minZ)
    G_minZ = _Z;
  if(_Z > G_maxZ)
    G_maxZ = _Z;
}


int32_t MPU6050_changeAccScale(MPU6050_ScaleAccTypeDef _scale)
{
	MPU6050_Send_Byte_To_Register(ACCEL_CONFIG,(_scale<<3));
	return 0;
}

int32_t MPU6050_changeGyroScale(MPU6050_ScaleGyroTypeDef _scale)
{
	MPU6050_Send_Byte_To_Register(GYRO_CONFIG,(_scale<<3));
	return 0;
}
int32_t MPU6050_setGyroDiv(int32_t _division)
{
	if (_division > 256 || _division < 1)
	{
		return -1;
	}


	MPU6050_Send_Byte_To_Register(SMPLRT_DIV,_division-1);
	return 0;
}

int32_t MPU6050_setDLPF(MPU6050_DLPFTypeDef _DLPF)
{
	MPU6050_Send_Byte_To_Register(CONFIG,_DLPF);
	return 0;
}
