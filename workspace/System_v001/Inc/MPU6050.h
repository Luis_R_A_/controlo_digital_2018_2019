


#ifndef __MPU_6050_H
#define __MPU_6050_H
//#include "stm32f0xx.h"
#include "stm32f7xx_hal.h"

#define I2C_TIMEOUT_ERROR 0xFFFFFFFF

#define MPU6050_ADDRESS (0x68<<1)

/* Configuration registers */
#define SMPLRT_DIV 25 //Sample Rate Divider
#define CONFIG 26 //Configuration
#define GYRO_CONFIG 27 //Gyroscope Configuration
#define ACCEL_CONFIG 28 //Accelerometer Configuration
#define MOT_THR 31 //Motion Detection Threshold
#define FIFO_EN 35 //FIFO Enable

/* Interrupt registers */
#define INT_PIN_CFG 55 //INT Pin / Bypass Enable Configuration - LATCH_INT_EN=1 is handy without using MCU interrupts
#define INT_ENABLE  56 //Interrupt Enable - data ready is usefull
#define INT_STATUS 58 //Interrupt Status - should be read to clear interrupts in default

/* Accel output registers*/
#define ACCEL_XOUT_H 59
#define ACCEL_XOUT_L 60
#define ACCEL_YOUT_H 61
#define ACCEL_YOUT_L 62
#define ACCEL_ZOUT_H 63
#define ACCEL_ZOUT_L 64

/* Temp output registers */
#define TEMP_OUT_H 65
#define TEMP_OUT_L 66

/* Gyro output registers */
#define GYRO_XOUT_H 67
#define GYRO_XOUT_L 68
#define GYRO_YOUT_H 69
#define GYRO_YOUT_L 70
#define GYRO_ZOUT_H 71
#define GYRO_ZOUT_L 72

/* */
#define MOT_DETECT_CTRL 105 //Motion Detection Control
#define USER_CTRL 106 //User Control
#define PWR_MGMT_1 107 //Power Management 1
#define PWR_MGMT_2 108 //Power Management 2
#define FIFO_COUNTH 114 //FIFO Count Registers  - high bytes
#define FIFO_COUNTL 115 //FIFO Count Registers - low bytes
#define FIFO_R_W  116 //FIFO Read Write
#define WHO_AM_I 117

#define DEFAULT_PWR_MGMT_1 0x00//0 0 0 0 0 00

#define DEFAULT_GYRO_SCALE 250 //+- �/s  -  1�/S = 131 - 1 bit = 7,634 milli �/s
#define DEFAULT_ACCEL_SCALE 2 //+- g  -  1g = 16388 - 1 bit= 61 milli-g
#define DEFAULT_GYRO_LSB 0.007634 //in �/s
#define DEFAULT_ACCEL_LSB 0.000061 //in g's


typedef enum
{
  MPU6050_SCALEGYRO_250   = 0x00U,
  MPU6050_SCALEGYRO_500   = 0x01U,
  MPU6050_SCALEGYRO_1000  = 0x02U,
  MPU6050_SCALEGYRO_2000  = 0x03U
} MPU6050_ScaleGyroTypeDef;
extern const double scaleGyroLSB[];

typedef enum
{
  MPU6050_SCALEACC_2  = 0x00U,
  MPU6050_SCALEACC_4  = 0x01U,
  MPU6050_SCALEACC_8  = 0x02U,
  MPU6050_SCALEACC_16 = 0x03U
} MPU6050_ScaleAccTypeDef;
extern const double scaleAccLSB[];


typedef enum
{
  MPU6050_DLPF_260_256  = 0x00U, //Gyro 8Khz
  MPU6050_DLPF_184_188  = 0x01U, //Gyro 1Khz
  MPU6050_DLPF_94_98  	= 0x02U, //Gyro 1Khz
  MPU6050_DLPF_44_42  	= 0x03U, //Gyro 1Khz
  MPU6050_DLPF_21_20  	= 0x04U, //Gyro 1Khz
  MPU6050_DLPF_10_10  	= 0x05U, //Gyro 1Khz
  MPU6050_DLPF_5_5  	= 0x06U //Gyro 1Khz
} MPU6050_DLPFTypeDef;

/*
  Functions prototypes
*/
int MPU6050_Init(I2C_HandleTypeDef *_handler);
int MPU6050_Set_Filters(int _n);

int MPU6050_Set_To_Reg(int _n);
int MPU6050_Read_Byte_From_Register(int _n);
int MPU6050_Send_Byte_To_Register(int _reg, uint8_t _data);

char MPU6050_Get_MyID(void);

int MPU6050_Get_Raw_Accel(int16_t* _X, int16_t* _Y, int16_t* _Z);
int MPU6050_Get_Raw_Gyro(int16_t* _X, int16_t* _Y, int16_t* _Z);
int MPU6050_Get_Gyro_Zero(int* _X, int* _Y, int* _Z);

int MPU6050_Int_Data_Ready_Enable(void);
int MPU6050_Data_Ready(void);

void MPU6050_Update_Accel_Max_Min(int _X, int _Y, int _Z);
void MPU6050_Update_Gyro_Max_Min(int _X, int _Y, int _Z);

int32_t MPU6050_changeAccScale(MPU6050_ScaleAccTypeDef _scale);
int32_t MPU6050_changeGyroScale(MPU6050_ScaleGyroTypeDef _scale);
int32_t MPU6050_setGyroDiv(int32_t _division);
int32_t MPU6050_setDLPF(MPU6050_DLPFTypeDef _DLPF);


#endif /* __MPU_6050_H */
